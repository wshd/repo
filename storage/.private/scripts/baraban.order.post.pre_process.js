if (event.request.body.record) {
	//	Loop through the record array and modify the data before it gets to the database.
	_.each(event.request.body.record, function(record, index, list) {
		record.uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
	});
}