### DreamFactory Enterprise(tm) Auditing Library v1.0.6

[![Latest Stable Version](https://poser.pugx.org/dreamfactory/fabric-audit/v/stable.svg)](https://packagist.org/packages/dreamfactory/fabric-audit) [![Total Downloads](https://poser.pugx.org/dreamfactory/fabric-audit/downloads.svg)](https://packagist.org/packages/dreamfactory/fabric-audit) [![Latest Unstable Version](https://poser.pugx.org/dreamfactory/fabric-audit/v/unstable.svg)](https://packagist.org/packages/dreamfactory/fabric-audit) [![License](https://poser.pugx.org/dreamfactory/fabric-audit/license.svg)](https://packagist.org/packages/dreamfactory/fabric-audit)

This library contains common components for interacting with DreamFactory Enterprise&trade;.

## Installation

Add a line to your "require" section in your composer configuration:

	"require":           {
		"dreamfactory/fabric-audit": "~1.0"
	}

Run a composer update:

    $ composer update

